import React, { Component } from 'react';
import MovieApp from "./containers/MovieApp";

class App extends Component {
  render() {
    return (
      <div className="container">
        <MovieApp />
      </div>
    );
  }
}

export default App;
