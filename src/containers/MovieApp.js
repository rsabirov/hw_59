import React, {Component} from 'react';
import Wrapper from '../hoc/Wrapper';
import Form from '../components/form-block/Form';
import Result from '../components/result/Result';

class MovieApp extends Component {

    state = {
        movies: [],
        currentName: '',
        lastId: 0,
        activeMovie: 0
    };

    changeNameHandler = (event) => {
        const currentName = event.target.value;
        this.setState({currentName});
    };

    setActiveMovie = (movieId) => {
        this.setState({
          activeMovie: movieId
        });
    };

    editName = (event) => {
        const name = event.target.value;

        const movies = this.state.movies.map(movie => {
           if (movie.id === this.state.activeMovie) {
             return {...movie, name}
           } else {
               return movie;
           }
        });

        this.setState({movies});
    };

    addMovie = () => {
        let movies = this.state.movies;

        let lastId = this.state.lastId + 1;

        const movie = {id: lastId, name: this.state.currentName};

        movies.push(movie);

        this.setState({movies, lastId});
    };

    removeMovie = (id) => {

        let movies = this.state.movies;
        const movie = movies.find(m => m.id === id);
        if (movie) {
            movies.splice(movies.indexOf(movie), 1);
        }

        this.setState({movies});
    };

    render() {
        return (
            <Wrapper>
                <Form changeName={this.changeNameHandler}
                      data={this.state.movies}
                      clickHandler={() => this.addMovie()}
                />
                <Result data={this.state.movies}
                        removeHandler={this.removeMovie}
                        edit={this.editName}
                        setActiveMovie={this.setActiveMovie}
                />
            </Wrapper>
        );
    }
}

export default MovieApp;
