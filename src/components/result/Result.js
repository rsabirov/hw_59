import React, {Component} from 'react';
import Movie from "./movie/Movie";

class Result extends Component {

   render() {
     const moviesArray = this.props.data.map((movie, index) =>{
       return (
         <div onClick={() => this.props.setActiveMovie(movie.id)} key={movie.id}>
           <Movie  nn={index + 1}
                   id={movie.id}
                   name={movie.name}
                   clickHandler={this.props.removeHandler}
                   edit={this.props.edit}
           />
         </div>
       )
     });

    return (
       <div className="result-block">
         <div className="result-block__items">
           To watch later:
          {moviesArray}
         </div>
       </div>
    )
  }
};

export default Result;