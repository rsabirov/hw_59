import React, {Component} from 'react';

class Movie extends Component{

  shouldComponentUpdate(nextProps) {
    console.log('Movie Did update');
    return nextProps.name !== this.props.name;
  }

  render() {
    return (
        <div className="row align-items-center result-block__item">
            <div className="col-md-10">
              <div className="row align-items-center">
                <div className="col-md-1">
                  {this.props.nn}
                </div>
                <div className="col-md-6">
                  {this.props.name}
                </div>
                <div className="col-md-5">
                  <input type="text" className="form-control" value={this.props.name} onChange={this.props.edit} />
                </div>
              </div>
            </div>
            <div className="col-md-2">
                <button type="button" className="btn btn-danger btn-block"
                        onClick={() => this.props.clickHandler(this.props.id)}>Delete
                </button>
            </div>
        </div>
    )
  }
};

export default Movie;