import React from 'react';

const Form = props => {
  return (
    <div className="form-block">
      <div className="row">
        <div className="col-md-10">
            <div className="input-group">
              <input type="text" className="form-control" placeholder="Item name" onChange={props.changeName} />
            </div>
        </div>
        <div className="col-md-2">
          <button type="button" className="btn btn-primary btn-block" onClick={props.clickHandler}>Add</button>
        </div>
      </div>
    </div>
  )
};

export default Form;